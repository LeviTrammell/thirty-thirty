package com.levitrammell.thirty_thirtytv.ui.channel_detail.view

import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.levitrammell.thirty_thirtytv.R
import com.levitrammell.thirty_thirtytv.databinding.ActivityChannelDetailBinding
import com.levitrammell.thirty_thirtytv.model.Guide
import com.levitrammell.thirty_thirtytv.model.LineupItem
import dagger.android.AndroidInjection
import javax.inject.Inject


class ChannelDetailActivity : AppCompatActivity(),  ChannelDetailMVPView {
    private lateinit var binding: ActivityChannelDetailBinding
    private lateinit var player: ExoPlayer

    private lateinit var mediaURL: String

    @Inject
    internal lateinit var guideEntryAdapter: GuideEntryAdapter

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val channel = intent.getParcelableExtra<Guide.Channel>("channel")
        val lineup = intent.getParcelableExtra<LineupItem>("lineup")

        mediaURL = lineup.url

        binding = DataBindingUtil.setContentView(this, R.layout.activity_channel_detail)
        binding.adapter = guideEntryAdapter
        binding.layoutManager = LinearLayoutManager(this)

        guideEntryAdapter.updateLineup(channel.entries)

        initializePlayer(binding.playerView)
    }

    override fun onStart() {
        super.onStart()

        val userAgent = Util.getUserAgent(this, this.getString(R.string.app_name))
        val mediaSource = ExtractorMediaSource
            .Factory(DefaultDataSourceFactory(this, userAgent))
            .setExtractorsFactory(DefaultExtractorsFactory())
            .createMediaSource(Uri.parse(mediaURL))
        player.prepare(mediaSource)
        player.playWhenReady = true
    }

    override fun onPause() {
        super.onPause()
        player.stop()
    }

    override fun onStop() {
        super.onStop()
        player.stop()
    }

    private fun initializePlayer(playerView: PlayerView) {
        val trackSelector = DefaultTrackSelector()
        val loadControl = DefaultLoadControl()
        val renderersFactory = DefaultRenderersFactory(this)

        player = ExoPlayerFactory.newSimpleInstance(
            renderersFactory, trackSelector, loadControl)

        playerView.player = player
    }
}