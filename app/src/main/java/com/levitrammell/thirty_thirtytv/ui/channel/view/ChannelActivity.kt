package com.levitrammell.thirty_thirtytv.ui.channel.view

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.levitrammell.thirty_thirtytv.R
import com.levitrammell.thirty_thirtytv.databinding.ActivityChannelBinding
import com.levitrammell.thirty_thirtytv.databinding.ActivityDeviceBinding
import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.model.Guide
import com.levitrammell.thirty_thirtytv.model.LineupItem
import com.levitrammell.thirty_thirtytv.ui.channel.presenter.ChannelMVPPresenter
import com.levitrammell.thirty_thirtytv.ui.device.presenter.DeviceMVPPresenter
import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceAdapter

import dagger.android.AndroidInjection
import javax.inject.Inject

class ChannelActivity : AppCompatActivity(), ChannelMVPView {
    private lateinit var binding: ActivityChannelBinding

    @Inject
    internal lateinit var channelAdapter: ChannelAdapter

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    @Inject
    internal lateinit var presenter: ChannelMVPPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_channel)
        binding.adapter = channelAdapter
        binding.layoutManager = LinearLayoutManager(this)

        val device = intent.getParcelableExtra<DeviceDiscover>("device")

        presenter.fetchChannels(device)
    }

    override fun displayChannels(channels: List<Pair<LineupItem, Guide.Channel>>) {
        channelAdapter.updateLineup(channels)
    }
}