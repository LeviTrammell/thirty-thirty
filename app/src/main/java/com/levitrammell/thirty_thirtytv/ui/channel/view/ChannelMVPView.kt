package com.levitrammell.thirty_thirtytv.ui.channel.view

import com.levitrammell.thirty_thirtytv.model.Guide
import com.levitrammell.thirty_thirtytv.model.LineupItem

interface ChannelMVPView {
    fun displayChannels(channels: List<Pair<LineupItem, Guide.Channel>>)
}