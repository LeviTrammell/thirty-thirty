package com.levitrammell.thirty_thirtytv.ui.channel.view

import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.levitrammell.thirty_thirtytv.R
import com.levitrammell.thirty_thirtytv.databinding.ItemChannelBinding
import com.levitrammell.thirty_thirtytv.model.Guide
import com.levitrammell.thirty_thirtytv.model.LineupItem
import com.levitrammell.thirty_thirtytv.ui.channel_detail.view.ChannelDetailActivity

class ChannelAdapter : RecyclerView.Adapter<ChannelAdapter.ChannelViewHolder>() {
    private var lineup: List<Pair<LineupItem, Guide.Channel>> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemChannelBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_channel, parent, false)
        return ChannelViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return lineup.size
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        val pair = lineup[position]
        holder.bind(pair.first, pair.second)
    }

    fun updateLineup(lineup: List<Pair<LineupItem, Guide.Channel>>) {
        this.lineup = lineup
        notifyDataSetChanged()
    }

    class ChannelViewHolder(private val binding: ItemChannelBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(lineupItem: LineupItem, channel: Guide.Channel) {
            binding.lineupItem = lineupItem
            binding.channel = channel
            binding.executePendingBindings()
            binding.root.setOnClickListener {
                // TODO : Create a delegate pattern to get this out of here
                val intent = Intent(it.context, ChannelDetailActivity::class.java)
                intent.putExtra("lineup", lineupItem)
                intent.putExtra("channel", channel)
                it.context.startActivity(intent)
            }
        }
    }
}