package com.levitrammell.thirty_thirtytv.ui.channel

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import com.levitrammell.thirty_thirtytv.ui.channel.presenter.ChannelMVPPresenter
import com.levitrammell.thirty_thirtytv.ui.channel.presenter.ChannelPresenter
import com.levitrammell.thirty_thirtytv.ui.channel.view.ChannelAdapter
import dagger.Module
import dagger.Provides

@Module
class ChannelActivityModule {
    @Provides
    internal fun provideChannelPresenter(presenter: ChannelPresenter)
            : ChannelMVPPresenter = presenter

    @Provides
    internal fun provideChannelAdapter(): ChannelAdapter = ChannelAdapter()

    @Provides
    internal fun provideLinearLayoutManager(context: Context): LinearLayoutManager = LinearLayoutManager(context)
}