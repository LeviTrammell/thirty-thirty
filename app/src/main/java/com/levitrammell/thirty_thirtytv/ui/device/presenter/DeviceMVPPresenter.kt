package com.levitrammell.thirty_thirtytv.ui.device.presenter

import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceAdapter
import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceMVPView

interface DeviceMVPPresenter {
    fun onViewCreated()
    fun onAttach(view: DeviceMVPView)
}