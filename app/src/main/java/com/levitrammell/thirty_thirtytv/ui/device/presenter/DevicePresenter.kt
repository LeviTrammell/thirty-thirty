package com.levitrammell.thirty_thirtytv.ui.device.presenter

import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.network.HDHomeRunDeviceAPI
import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceAdapter
import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceMVPView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DevicePresenter @Inject internal constructor(private val hdHomeRunDeviceAPI: HDHomeRunDeviceAPI) : DeviceMVPPresenter {
    private var view: DeviceMVPView? = null

    override fun onAttach(view: DeviceMVPView) {
        this.view = view
    }

    override fun onViewCreated() {
        hdHomeRunDeviceAPI.findDevices()
            .flatMap {
                Observable.zip(
                    it.map {
                        hdHomeRunDeviceAPI.discover(it.discoverURL)
                    }) {
                        // TODO : Figure out how to make this less ugly. I'd rather not typecast unsafely
                        it.toList() as List<DeviceDiscover>
                    }
                }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                view?.displayDevices(it)
            }
    }
}