package com.levitrammell.thirty_thirtytv.ui.channel_detail.view

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.levitrammell.thirty_thirtytv.R
import com.levitrammell.thirty_thirtytv.databinding.ItemGuideEntryBinding
import com.levitrammell.thirty_thirtytv.model.Guide

class GuideEntryAdapter : RecyclerView.Adapter<GuideEntryAdapter.GuideEntryViewHolder>() {
    private var guideEntries: List<Guide.Entry> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuideEntryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemGuideEntryBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_guide_entry, parent, false)
        return GuideEntryViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return guideEntries.size
    }

    override fun onBindViewHolder(holder: GuideEntryViewHolder, position: Int) {
        holder.bind(guideEntries[position])
    }

    fun updateLineup(guideEntries: List<Guide.Entry>) {
        this.guideEntries = guideEntries
        notifyDataSetChanged()
    }

    class GuideEntryViewHolder(private val binding: ItemGuideEntryBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(guideEntry: Guide.Entry) {
            binding.guideEntry = guideEntry
            binding.executePendingBindings()
        }
    }
}