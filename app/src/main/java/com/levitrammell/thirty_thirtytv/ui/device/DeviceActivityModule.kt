package com.levitrammell.thirty_thirtytv.ui.device

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import com.levitrammell.thirty_thirtytv.ui.device.presenter.DeviceMVPPresenter
import com.levitrammell.thirty_thirtytv.ui.device.presenter.DevicePresenter
import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceAdapter
import dagger.Module
import dagger.Provides

@Module
class DeviceActivityModule {
    @Provides
    internal fun provideDevicePresenter(presenter: DevicePresenter)
            : DeviceMVPPresenter = presenter

    @Provides
    internal fun provideDeviceAdapter(): DeviceAdapter = DeviceAdapter()

    @Provides
    internal fun provideLinearLayoutManager(context: Context): LinearLayoutManager = LinearLayoutManager(context)
}