package com.levitrammell.thirty_thirtytv.ui.channel.presenter

import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.ui.channel.view.ChannelMVPView

interface ChannelMVPPresenter {
    fun fetchChannels(device: DeviceDiscover)
    fun onAttach(view: ChannelMVPView)
}