package com.levitrammell.thirty_thirtytv.ui.device.view

import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.levitrammell.thirty_thirtytv.R
import com.levitrammell.thirty_thirtytv.databinding.ItemDeviceBinding
import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.ui.channel.view.ChannelActivity

class DeviceAdapter : RecyclerView.Adapter<DeviceAdapter.DeviceViewHolder>() {
    private var devices: List<DeviceDiscover> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemDeviceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_device, parent, false)
        return DeviceViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return devices.size
    }

    override fun onBindViewHolder(holder: DeviceViewHolder, position: Int) {
        holder.bind(devices[position])
    }

    fun updateDevices(devices: List<DeviceDiscover>) {
        this.devices = devices
        notifyDataSetChanged()
    }


    class DeviceViewHolder(private val binding: ItemDeviceBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(device: DeviceDiscover) {
            binding.device = device
            binding.executePendingBindings()
            binding.root.setOnClickListener {
                // TODO : Create a delegate pattern to get this out of here
                val intent = Intent(it.context, ChannelActivity::class.java)
                intent.putExtra("device", device)
                it.context.startActivity(intent)
            }
        }
    }
}