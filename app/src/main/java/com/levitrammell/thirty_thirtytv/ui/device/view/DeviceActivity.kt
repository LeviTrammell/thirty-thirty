package com.levitrammell.thirty_thirtytv.ui.device.view

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.levitrammell.thirty_thirtytv.R
import com.levitrammell.thirty_thirtytv.databinding.ActivityDeviceBinding
import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.ui.device.presenter.DeviceMVPPresenter
import dagger.android.AndroidInjection
import javax.inject.Inject


class DeviceActivity : AppCompatActivity(), DeviceMVPView {
    private lateinit var binding: ActivityDeviceBinding

    @Inject
    internal lateinit var deviceAdapter: DeviceAdapter

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    @Inject
    internal lateinit var presenter: DeviceMVPPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        presenter.onAttach(this)
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.setContentView(this, R.layout.activity_device)
        binding.adapter = deviceAdapter
        binding.layoutManager = LinearLayoutManager(this)
        binding.dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)

        presenter.onViewCreated()
    }

    override fun displayDevices(devices: List<DeviceDiscover>) {
        deviceAdapter.updateDevices(devices)
    }
}
