package com.levitrammell.thirty_thirtytv.ui.channel_detail

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import com.levitrammell.thirty_thirtytv.ui.channel_detail.view.GuideEntryAdapter
import dagger.Module
import dagger.Provides

@Module
class ChannelDetailActivityModule {
    @Provides
    internal fun provideGuideEntryAdapter(): GuideEntryAdapter = GuideEntryAdapter()

    @Provides
    internal fun provideLinearLayoutManager(context: Context): LinearLayoutManager = LinearLayoutManager(context)
}