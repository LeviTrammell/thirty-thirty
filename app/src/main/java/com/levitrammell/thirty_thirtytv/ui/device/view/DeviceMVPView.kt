package com.levitrammell.thirty_thirtytv.ui.device.view

import com.levitrammell.thirty_thirtytv.model.DeviceDiscover

interface DeviceMVPView {
    fun displayDevices(devices: List<DeviceDiscover>)
//    fun openChannelActivity(device: DeviceDiscover)
}