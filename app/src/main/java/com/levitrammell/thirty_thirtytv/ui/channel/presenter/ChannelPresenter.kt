package com.levitrammell.thirty_thirtytv.ui.channel.presenter

import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.model.Guide
import com.levitrammell.thirty_thirtytv.model.LineupItem
import com.levitrammell.thirty_thirtytv.network.HDHomeRunDeviceAPI
import com.levitrammell.thirty_thirtytv.network.HDHomeRunGuideAPI
import com.levitrammell.thirty_thirtytv.ui.channel.view.ChannelMVPView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ChannelPresenter @Inject internal constructor(private val deviceAPI: HDHomeRunDeviceAPI,
                                                    private val guideAPI: HDHomeRunGuideAPI) : ChannelMVPPresenter {
    private var view: ChannelMVPView? = null

    override fun onAttach(view: ChannelMVPView) {
        this.view = view
    }

    override fun fetchChannels(device: DeviceDiscover) {
        Observable.zip(deviceAPI.getLineup(device.lineupURL),
            guideAPI.getGuide(device.deviceAuth),
            BiFunction<List<LineupItem>, List<Guide.Channel>, List<Pair<LineupItem, Guide.Channel>>> {
                lineupItems, channels ->
                // TODO : This is unsafe.
                val sortedLineupItems = lineupItems.sortedBy { it.channelNumber }
                val sortedChannels = channels.sortedBy { it.channelNumber }
                sortedLineupItems.mapIndexed { index, lineupItem -> Pair(lineupItem, sortedChannels[index]) }
            }).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                view?.displayChannels(it)
            }
    }
}