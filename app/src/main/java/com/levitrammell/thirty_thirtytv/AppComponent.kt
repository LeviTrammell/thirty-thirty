package com.levitrammell.thirty_thirtytv

import android.app.Application
import com.levitrammell.thirty_thirtytv.network.HDHomeRunGuideNetworkModule
import com.levitrammell.thirty_thirtytv.network.HDHomeRunNetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (AppModule::class), (HDHomeRunNetworkModule::class), (HDHomeRunGuideNetworkModule::class), (ActivityBuilder::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        fun lanModule(lanModule: HDHomeRunNetworkModule): Builder
        fun guideAPIModule(guideAPIModule: HDHomeRunGuideNetworkModule): Builder
        fun appModule(appModule: AppModule): Builder

        @BindsInstance
        fun application(application: Application): Builder
    }

    fun inject(app: ThirtyThirtyTVApplication)

}