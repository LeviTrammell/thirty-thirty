package com.levitrammell.thirty_thirtytv.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

class Guide {
    @Parcelize
    data class Channel(@Json(name = "GuideNumber") val channelNumber: String,
                       @Json(name = "GuideName") val callSign: String,
                       @Json(name = "Affiliate") val affiliate: String?,
                       @Json(name = "ImageURL") val imageURL: String,
                       @Json(name = "Guide") val entries: List<Entry>): Parcelable

    @Parcelize
    data class Entry(@Json(name = "StartTime") val startTime: Int,
                     @Json(name = "EndTime") val endTime: Int,
                     @Json(name = "Title") val title: String,
                     @Json(name = "OriginalAirdate") val originalAirdate: Int,
                     @Json(name = "ImageURL") val imageURL: String,
                     @Json(name = "SeriesID") val seriesID: String,
                     @Json(name = "Filter") val tags: List<String>?): Parcelable
}