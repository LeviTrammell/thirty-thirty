package com.levitrammell.thirty_thirtytv.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LineupItem(@Json(name = "GuideNumber") val channelNumber: String,
                      @Json(name = "GuideName") val callSign: String,
                      @Json(name = "HD") val isHD: Int,
                      @Json(name = "URL") val url: String): Parcelable