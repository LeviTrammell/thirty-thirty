package com.levitrammell.thirty_thirtytv.model

import com.squareup.moshi.Json

data class DeviceBase(@Json(name = "DeviceID") val deviceID: String,
                      @Json(name = "LocalIP") val localIP: String,
                      @Json(name = "DiscoverURL") val discoverURL: String)