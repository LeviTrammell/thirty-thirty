package com.levitrammell.thirty_thirtytv.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeviceDiscover(@Json(name = "FriendlyName") val name: String,
                          @Json(name = "ModelNumber") val modelNumber: String,
                          @Json(name = "FirmwareName") val firmwareName: String,
                          @Json(name = "FirmwareVersion") val firmwareVersion: String,
                          @Json(name = "DeviceID") val deviceID: String,
                          @Json(name = "DeviceAuth") val deviceAuth: String,
                          @Json(name = "TunerCount") val tunerCount: Int,
                          @Json(name = "LineupURL") val lineupURL: String) : Parcelable

