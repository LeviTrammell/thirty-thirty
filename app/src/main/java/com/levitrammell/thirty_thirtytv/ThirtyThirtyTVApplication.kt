package com.levitrammell.thirty_thirtytv

import android.app.Activity
import android.app.Application
import com.levitrammell.thirty_thirtytv.network.HDHomeRunGuideNetworkModule
import com.levitrammell.thirty_thirtytv.network.HDHomeRunNetworkModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class ThirtyThirtyTVApplication: Application(), HasActivityInjector {
    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = activityDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .lanModule(HDHomeRunNetworkModule)
            .guideAPIModule(HDHomeRunGuideNetworkModule)
            .application(this)
            .build()
            .inject(this)
    }
}