package com.levitrammell.thirty_thirtytv

import com.levitrammell.thirty_thirtytv.ui.channel.ChannelActivityModule
import com.levitrammell.thirty_thirtytv.ui.channel.view.ChannelActivity
import com.levitrammell.thirty_thirtytv.ui.channel_detail.ChannelDetailActivityModule
import com.levitrammell.thirty_thirtytv.ui.channel_detail.view.ChannelDetailActivity
import com.levitrammell.thirty_thirtytv.ui.device.DeviceActivityModule
import com.levitrammell.thirty_thirtytv.ui.device.view.DeviceActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [(DeviceActivityModule::class)])
    abstract fun bindDeviceActivity(): DeviceActivity

    @ContributesAndroidInjector(modules = [(ChannelActivityModule::class)])
    abstract fun bindChannelActivity(): ChannelActivity

    @ContributesAndroidInjector(modules = [(ChannelDetailActivityModule::class)])
    abstract fun bindChannelDetailActivity(): ChannelDetailActivity
}