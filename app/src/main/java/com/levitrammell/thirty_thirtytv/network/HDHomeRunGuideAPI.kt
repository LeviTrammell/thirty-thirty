package com.levitrammell.thirty_thirtytv.network

import com.levitrammell.thirty_thirtytv.model.Guide
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface HDHomeRunGuideAPI {
    @GET("/api/guide.php")
    fun getGuide(@Query("DeviceAuth", encoded = true) deviceAuth: String): Observable<List<Guide.Channel>>
}