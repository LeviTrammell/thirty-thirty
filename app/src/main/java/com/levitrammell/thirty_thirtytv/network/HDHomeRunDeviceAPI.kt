package com.levitrammell.thirty_thirtytv.network

import com.levitrammell.thirty_thirtytv.model.DeviceBase
import com.levitrammell.thirty_thirtytv.model.DeviceDiscover
import com.levitrammell.thirty_thirtytv.model.LineupItem
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface HDHomeRunDeviceAPI {
    @GET("/discover")
    fun findDevices(): Observable<List<DeviceBase>>

    @GET()
    fun discover(@Url url: String): Observable<DeviceDiscover>

    @GET()
    fun getLineup(@Url url: String): Observable<List<LineupItem>>
}