package com.levitrammell.thirty_thirtytv.network


import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named

@Module
object HDHomeRunNetworkModule {

    @Provides
    @Reusable
    internal fun provideHDHomeRunWANAPI(@Named("HDHomeRunLANAPIRetrofit") retrofit: Retrofit): HDHomeRunDeviceAPI {
        return retrofit.create(HDHomeRunDeviceAPI::class.java)
    }

    @Provides
    @Named("HDHomeRunLANAPIRetrofit")
    @Reusable
    internal fun provideRetrofitInterface(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://ipv4-api.hdhomerun.com")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

}