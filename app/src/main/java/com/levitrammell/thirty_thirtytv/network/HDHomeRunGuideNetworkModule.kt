package com.levitrammell.thirty_thirtytv.network

import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor



@Module
object HDHomeRunGuideNetworkModule {

    @Provides
    @Reusable
    internal fun provideHDHomeRunGuideAPI(@Named("HDHomeRunGuideAPIRetrofit") retrofit: Retrofit): HDHomeRunGuideAPI {
        return retrofit.create(HDHomeRunGuideAPI::class.java)
    }

    @Provides
    @Named("HDHomeRunGuideAPIRetrofit")
    @Reusable
    internal fun provideRetrofitInterface(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .baseUrl("http://my.hdhomerun.com")
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

}